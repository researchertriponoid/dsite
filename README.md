# OrangeFox Recovery | Downloads Repo

This project is created using NextJS, React, Material UI, React-Intl, SASS and Typescript

### Features:

- SSR support and SEO friendly
- Includes structured data/json support
- Fast and responsive
- Fully translated, thanks to our community
- Dynamic sitemap generation

### Setup Instructions (`package.json` > `scripts`)

First `cd src` to get into the project, and then:

_Recommendation: use **Yarn** instead of **NPM**_

- Install all packages

  > `yarn` or `npm i`

- Run the application with Nodemon support

  > `yarn dev` or `npm run dev`

- Run the application to support easy debugging using VSCode

  > `yarn next` or `npm run next`

- Build the application with Server bundle

  > `yarn build` or `npm run build`

- Start the application with Server bundle

  > `yarn start` or `npm run start`

- Serve the application with Server bundle (clubs both Build and Start scripts)
  > `yarn server` or `npm run server`

### Related Links

- [Wiki](https://wiki.orangefox.tech/)
- [Website Downloads](https://orangefox.download/)
- [Crowdin Translations](https://translate.orangefox.tech/downloads-website)
- `Telegram` [Support](https://t.me/OrangeFoxChat)
- `Telegram` [News](https://t.me/OrangeFoxNEWS)
