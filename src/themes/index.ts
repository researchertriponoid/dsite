import { createMuiTheme } from "@material-ui/core/styles";
import darkTheme from './dark';
import lightTheme from './light';

export const DarkTheme = createMuiTheme(darkTheme);
export const LightTheme = createMuiTheme(lightTheme);
