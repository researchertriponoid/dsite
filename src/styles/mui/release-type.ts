import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
    root: {
        width: "100%",
        backgroundColor: "#2a2a2a",
    },
    icon: {
        color: "#ddd",
    },
    iconM5: {
        color: "#ddd",
        marginRight: "5px",
    },
    details: {
        display: "flex",
        padding: "5px 10px 10px",
        flexDirection: "column",
    },
}));
