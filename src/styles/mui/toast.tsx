import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  toast: {
    color: "var(--white-1)",
    backgroundColor: "var(--grey-4)",
  },
}));
